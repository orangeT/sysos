<html>
<head>
    <title>SysOS - Resultado</title>
    <link rel="icon" href="img/favicon.ico" type="image/x-icon" />
</head>
<body>
<?php
error_reporting(0);
header('Content-Type: text/html; charset=utf-8');
function verify($var){
    if (isset($var) && $var != '0' && $var != '') {
        return True;
    }else {
        return False;
    }
}

function listCpe($tipo){
    if (verify($_POST['cpe'.$tipo])){
        printf("%02d CPE 5.8Ghz<br>", $_POST['cpe'.$tipo]);
    }if (verify($_POST['bullet'.$tipo])){
        printf("%02d Bullet %sGhz<br>", $_POST['bullet'.$tipo], $_POST['bulletGhz'.$tipo]);
    }if (verify($_POST['fontPoe'.$tipo])){
        printf("%02d fonte POE<br>", $_POST['fontPoe'.$tipo]);
    }if (verify($_POST['mtsPoe'.$tipo])){
        printf("%02d metros de cabo POE<br>", $_POST['mtsPoe'.$tipo]);
    }if (verify($_POST['mtsLan'.$tipo])) {
        printf("%.1f metros de cabo LAN<br>", $_POST['mtsLan'.$tipo]);
    }if (verify($_POST['sup'.$tipo])){
        printf("%02d suporte %s<br>", $_POST['sup'.$tipo], $_POST['supDes'.$tipo]);
    }if (verify($_POST['barra'.$tipo])) {
        printf("%02d barra de %.1fMts<br>", $_POST['barra'.$tipo], $_POST['mtsBar'.$tipo]);
    }if (verify($_POST['rj'.$tipo])) {
        printf("%02d RJ45<br>", $_POST['rj'.$tipo]);
    }if (verify($_POST['estrela'.$tipo])) {
        printf("%02d estrela<br>", $_POST['estrela'.$tipo]);
    }if (verify($_POST['otCpe'.$tipo])) {
        $otCpeInst = str_replace("\n", "<br>", $_POST['otCpe'.$tipo]);
        print($otCpeInst."<br>");
    }
    print("<br>");
}

function listFibra($tipo){
    if (verify($_POST['onu'.$tipo])){
        printf("%02d ONU %s<br>", $_POST['onu'.$tipo], $_POST['onuMarca'.$tipo]);
    }if (verify($_POST['fontOnu'.$tipo])){
        printf("%02d fonte ONU<br>", $_POST['fontOnu'.$tipo]);
    }if (verify($_POST['mtsFib'.$tipo])){
        printf("%02d metros de Fibra<br>", $_POST['mtsFib'.$tipo]);
    }if (verify($_POST['mtsLanFib'.$tipo])) {
        printf("%.1f metros de cabo LAN<br>", $_POST['mtsLanFib'.$tipo]);
    }if (verify($_POST['roteador'.$tipo])) {
        printf("%02d roteador<br>", $_POST['roteador'.$tipo]);
    }if (verify($_POST['fontRot'.$tipo])){
        printf("%02d fonte Roteador<br>", $_POST['fontRot'.$tipo]);
    }if (verify($_POST['conec'.$tipo])) {
        printf("%02d conectores<br>", $_POST['conec'.$tipo]);
    }if (verify($_POST['otFib'.$tipo])) {
        $otCpeInst = str_replace("\n", "<br>", $_POST['otFib'.$tipo]);
        print($otCpeInst."<br>");
    }
    print("<br>");
}

function listRetirada(){
    printf("Os equipamentos foram retirados %s.<br><br>", $_POST['comodoRet']);
    if (count($_POST['fibRetChk']) > 0){
        $fibRetStr = join(", ", $_POST['fibRetChk']);
        printf("Fibra:<br> Foram retirados os seguintes equipamentos: %s.<br><br>", $fibRetStr);
    }if (count($_POST['cpeRetChk']) > 0){
        $cpeRetStr = join(", ", $_POST['cpeRetChk']);
        printf("CPE:<br> Foram retirados os seguintes equipamentos: %s.<br><br>", $cpeRetStr);
    }
    if(verify($_POST['otherRetChk'])){
        printf("Outros equipamentos retirado: %s", $_POST['otherRetChk']);
    }
    if (isset($_POST['lacreNV'])){
        if ($_POST['lacreNV'] == "1"){
            echo "Foi identificado lacre com adesivo na NV/ND";
        }elseif ($_POST['lacreNV'] == "0"){
            echo "Não foi identificado lacre com adesivo na NV/ND";
        }
        echo "<br><br>";
    }
    if ($_POST['allEquip'] == "0"){
        echo "Não foram retirados todos os equipamentos.<br>";
        printf("Equipamentos não retirados: %s.<br>", $_POST['equipNotRet']);
    }elseif ($_POST['allEquip'] == "1"){
        echo "Foram retirados todos os equipamentos.<br>";
    }
    echo "<br>";
}

$tipo = $_POST['tipo'];
if (verify($_POST['obs'])) {
    $obs = str_replace("\n", "<br>", $_POST['obs']);
    print($obs."<br>");
}
if (verify($_POST['valor']) && $_POST['valor'] != "R$ 0,00"){
    printf("Valor: %s<br>", str_replace(",", "", $_POST['valor']));
}print("<br>");
if ($tipo == "0"){
    if ($_POST['problem'] == "0") {
        $problemDes = str_replace("\n", "<br>", $_POST['problemDes']);
        printf("Problema identificado: %s<br><br>", $problemDes);
    }else{
        $noproblemDes = str_replace("\n", "<br>", $_POST['noproblemDes']);
        printf("Problema não identificado: %s<br><br>", $noproblemDes);
    }
}elseif ($tipo == "1") {
    if ($_POST['inst'] == "0") {
        print("Equipamentos Instalados - CPE<br>");
        listCpe('Inst');
    }elseif ($_POST['inst'] == "1") {
        print("Equipamentos Instalados - Fibra<br>");
        listFibra('Inst');
    }
    printf("Os equipamentos foram instalados %s.<br><br>", $_POST['comodo']);
}elseif ($tipo == "2"){
    $troca = $_POST['troca'];
    if ($troca == "0"){
        print("Equipamentos Retirados - CPE<br>");
        listCpe('Ret');
        print("Equipamentos Instalados - CPE<br>");
        listCpe('Inst');
    }elseif ($troca == "1"){
        print("Equipamentos Retirados - CPE<br>");
        listCpe('Ret');
        print("Equipamentos Instalados - Fibra<br>");
        listFibra('Inst');
    }elseif ($troca == "2"){
        print("Equipamentos Retirados - Fibra<br>");
        listFibra('Ret');
        print("Equipamentos Instalados - Fibra<br>");
        listFibra('Inst');
    }
    printf("Os equipamentos foram instalados %s.<br><br>", $_POST['comodo']);
}elseif ($tipo == "3"){
    if ($_POST['retirRea'] == "1"){
        echo "Retirada realizada<br><br>";
        listRetirada();
        echo "<br>";
    }elseif ($_POST['retirRea'] == "0"){
        echo "Retirada não realizada<br><br>";
    }
}
if (isset($_POST['tentativa'])){
    $qtdVisit = count($_POST['dateTryVisit']);
    for ($i = 0; $i < $qtdVisit; $i++){
        $dateTryVisit[$i] = date_create($_POST['dateTryVisit'][$i]);
        $tecHr[$i] = str_replace("\n", "<br>", $_POST['tecTryVisit'][$i]);
        printf("Data: %s Hora: %s<br>", date_format($dateTryVisit[$i], 'd/m/Y'), $_POST['hourTryVisit'][$i]);
        printf("Técnico: %s<br>", $tecHr[$i]);
        if ($_POST['retirTryVisit'][$i] == "0"){
            echo "Equipamento não foi retirado<br>";
            if(verify($_POST['reasonTryVisit'][$i])){
                printf("Por que não retirou? %s<br>", $_POST['reasonTryVisit'][$i]);
            }if ($_POST['vizinhoTryVisit'][$i] == "0"){
                echo "Vizinho não soube dar informação.<br>";
            }elseif ($_POST['vizinhoTryVisit'][$i] == "1"){
                echo "Vizinho deu informação.<br>";
                if (verify($_POST['infoTryVisit'][$i])){
                    printf("Informação: %s<br>", $_POST['infoTryVisit'][$i]);
                }if (verify($_POST['parTryVisit'][$i])){
                    printf("Parentesco de quem deu informação: %s<br>", $_POST['parTryVisit'][$i]);
                }if (verify($_POST['addressTryVisit'][$i])){
                    printf("Endereço de quem deu informação: %s<br>", $_POST['addressTryVisit'][$i]);
                }if (verify($_POST['vizinhoNameTryVisit'][$i])){
                    printf("Foi falado com %s<br>", $_POST['vizinhoNameTryVisit'][$i]);
                }
            }
        }
        echo "<br>";
    }
    $qtdCall = count(array_filter($_POST['telTryCall']));
    $dateTryCall[$i] = date_create($_POST['dateTryCall'][$i]);
    for ($i = 0; $i < $qtdCall; $i++){
        printf("Telefone: %s<br>", $_POST['telTryCall'][$i]);
        if (verify($_POST['resTryCall'][$i])){
            printf("Resultado da ligação: %s<br>", $_POST['resTryCall'][$i]);
        }
        printf("Data: %s Hora: %s<br>", date_format($dateTryCall[$i], 'd/m/Y'), $_POST['hourTryCall'][$i]);
        if (verify($_POST['tecTryCall'][$i])){
            printf("Funcionario: %s<br>", $_POST['tecTryCall'][$i]);
        }if (verify($_POST['nameTryCall'])){
            printf("Falou com %s<br>", $_POST['nameTryCall'][$i]);
        }if (verify($_POST['parTryCall'][$i])){
            printf("Parentesco: %s<br>", $_POST['parTryCall'][$i]);
        }
    }
    echo "<br>";
    if ($_POST['remarked'] == "1"){
        echo "Foi agendada retirada.<br>";
    }elseif ($_POST['remarked'] == "0"){
        echo "Não foi agendado retirada.<br>";
    }if(verify($_POST['provid'])){
        printf("Providências a serem tomadas: %s<br>", $_POST['provid']);
    }
}
if ($tipo != "3"){
    if (verify($_POST['nv'] || $_POST['if'] || $_POST['p'] || $_POST['pp'] || $_POST['pc'] || $_POST['speedD'] || $_POST['speedU'] || $_POST['ping'] || $_POST['mac'] || $_POST['equipSe'])) {
        print("Informações técnicas:<br>");
        if (verify($_POST['nv']) && $tipo != "0"){
            printf("NV/ND %s<br>", str_replace("_", "", $_POST['nv']));
        }if (verify($_POST['if']) && $tipo == "1"){
            printf("Interface: %s<br>", $_POST['if']);
        }if (verify($_POST['p']) && $tipo != "0"){
            printf("P %02d<br>", $_POST['p']);
        }if (verify($_POST['pp']) && $tipo != "0"){
            printf("PP %02.2f<br>", $_POST['pp']);
        }if (verify($_POST['pc']) && $tipo != "0"){
            printf("PC %02.2f<br>", $_POST['pc']);
        }if (verify($_POST['speedD']) && !verify($_POST['speedU']) && verify($_POST['ping'])) {
            printf("Velocidade: %s Ping: %s<br>", $_POST['speedD'], $_POST['ping']);
        }elseif (verify($_POST['speedD']) && verify($_POST['speedU']) && verify($_POST['ping'])){
            printf("Velocidade Download: %s Velocidade Upload: %s Ping: %s<br>", $_POST['speedD'], $_POST['speedU'], $_POST['ping']);
        }elseif (verify($_POST['speedD']) && verify($_POST['speedU'])){
            printf("Velocidade Download: %s Velocidade Upload: %s<br>", $_POST['speedD'], $_POST['speedU']);
        }elseif (verify($_POST['speedD']) && !verify($_POST['ping'])){
            printf("Velocidade: %s<br>", $_POST['speedD']);
        }elseif (!verify($_POST['speedD']) && verify($_POST['ping'])){
            printf("Ping: %s<br>", $_POST['ping']);
        }if (verify($_POST['mac']) && $tipo != "0"){
            printf("MAC: %s<br>", $_POST['mac']);
        }if (verify($_POST['equipSe']) && $tipo != "0" && (!isset($troca) || $troca != "0") && (!isset($_POST['inst']) || $_POST['inst'] != "0")){
        printf("Nº Série Equip: %s<br>", $_POST['equipSe']);
    }
    print("<br>");
    }
    
}
if (verify($_POST['dataI'] && $_POST['horaI'] && $_POST['dataT'] && $_POST['horaI'])){
    $dataI = date_create($_POST['dataI']);
    $dataT = date_create($_POST['dataT']);
    printf("Data de início: %s<br>Hora da chegada: %s<br>Data de Término: %s<br>Hora da Saída: %s<br><br>", date_format($dataI, 'd/m/Y'), $_POST['horaI'], date_format($dataT, 'd/m/Y'), $_POST['horaT']);
}elseif(verify($_POST['dataI'] && $_POST['dataT'])){
    $dataI = date_create($_POST['dataI']);
    $dataT = date_create($_POST['dataT']);
    printf("Data de início: %s<br>Data de Término: %s<br><br>", date_format($dataI, 'd/m/Y'), date_format($dataT, 'd/m/Y'));
}
if (verify($_POST['tecInt'])) {
    printf("Técnico do Suporte Interno: %s<br>", $_POST['tecInt']);
}
if (count(array_filter($_POST['tecExt'])) > 0){
    $tecExt = join("<br>", $_POST['tecExt']);
    printf("Técnico Externo: %s<br><br>", $tecExt);
}
if (verify($_POST['cliName'])){
    printf("Cliente <br>Nome: %s<br>", $_POST['cliName']);
}if (verify($_POST['cliPar'])){
    printf("Parentesco: %s<br>", $_POST['cliPar']);
}
?>
</body>
</html>
