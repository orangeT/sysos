$(document).ready(function () {
    $("input.valor").inputmask({alias: 'numeric', radixPoint: ".", groupSeparator: ",", autoGroup: true, 'digits': 2, 'digitsOptional': false, 'placeholder': '0', prefix: "R$ "});
    $("input.mac").inputmask({mask: "##:##:##:##:##:##"});
    $("input.pot").inputmask({mask: "99.99", placeholder: "0", insert: true});
    $("input.nv").inputmask("9-9{2,3}-9{2,3}").focusout(function (event) {
        var target, nv, element;
        target = (event.currentTarget) ? event.currentTarget : event.srcElement;
        nv = target.value.replace('-', '').replace('_', '');
        element = $(target);
        if(nv.length === 5) {
            element.inputmask("9-99-99");
        }else if (nv.length === 7){
            element.inputmask("9-99-999");
        }else if (nv.length === 8){
            element.inputmask("9-999-999");
        }else if (nv.length === 4){
            element.inputmask("3-99-99");
        }
    }).focus(function (event) {
        var target, element;
        target = (event.currentTarget) ? event.currentTarget : event.srcElement;
        element = $(target);
        element.inputmask("9-9{2,3}-9{2,3}");
    });
    $(".mac").focusout(function(){
        var lastFive = this.value.slice(-5).replace(":", "");
        $("#serie").val(lastFive);
    });
});
