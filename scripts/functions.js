var contTryVisit = 1;
var contTryCall = 0;

function aparecer(id){
    document.getElementById(id).classList.remove("hide");
    document.getElementById(id).className += " show";
}
function desaparecer(id){
    document.getElementById(id).classList.remove("show");
    document.getElementById(id).className += " hide";
}
function desaparecerClass(cla){
    var item = document.getElementsByClassName(cla);
    for(var i = 0, length = item.length; i < length; i++){
        item[i].classList.remove("show");
        item[i].className += " hide";
    }
}
function aparecerClass(cla){
    var item = document.getElementsByClassName(cla);
    for(var i = 0, length = item.length; i < length; i++){
        item[i].classList.remove("hide");
        item[i].className += " show";
    }
}
function tip(va){
    if (va === "0") {
        desaparecerClass("suporte");
        desaparecerClass("retirada");
        desaparecerClass("instalacao");
        desaparecerClass("troca");
        desaparecerClass("opRet");
        aparecerClass("suporte");
    }else if (va === "1"){
        desaparecerClass("suporte");
        desaparecerClass("retirada");
        desaparecerClass("instalacao");
        desaparecerClass("troca");
        desaparecerClass("opRet");
        aparecer("tipInst");
    }else if (va === "2") {
        desaparecerClass("suporte");
        desaparecerClass("retirada");
        desaparecerClass("instalacao");
        desaparecerClass("troca");
        desaparecerClass("opRet");
        aparecer("tipTroca");
    }else if(va === "3"){
        desaparecerClass("suporte");
        desaparecerClass("retirada");
        desaparecerClass("instalacao");
        desaparecerClass("troca");
        aparecer("retFeita");
        aparecerClass("opRet");
    }
}
function tipTroc(va){
    document.getElementById('equip').style.width = "1000px";
    aparecerClass("troca");
    if (va === "0") {
        desaparecerClass("fib");
        desaparecer("fibInstalado");
        aparecer("cpeInstalado");
        desaparecer("fibRetirado");
        aparecer("cpeRetirado");
        aparecerClass("cpe");
    } else if (va === "1") {
        desaparecerClass("cpe");
        aparecerClass("fib");
        aparecer("fibInstalado");
        desaparecer("cpeInstalado");
        desaparecer("fibRetirado");
        aparecer("cpeRetirado");
    } else if (va === "2") {
        desaparecerClass("cpe");
        aparecerClass("fib");
        aparecer("fibInstalado");
        desaparecer("cpeInstalado");
        aparecer("fibRetirado");
        desaparecer("cpeRetirado");
    }
}
function tipIns(va) {
    document.getElementById('equip').style.width = "500px";
    desaparecer("fibRetirado");
    desaparecer("cpeRetirado");
    aparecerClass("instalacao");
    if (va === "0") {
        desaparecer("fibInstalado");
        aparecerClass("cpe");
        aparecer("cpeInstalado");
        desaparecerClass("fib");
    } else if (va === "1") {
        desaparecerClass("cpe");
        aparecerClass("fib");
        aparecer("fibInstalado");
        desaparecer("cpeInstalado");
    }
}

function retirada(va) {
    if (va === "1"){
        desaparecerClass("retiradaN");
        aparecerClass("retiradaS");
    }else if(va === "0"){
        desaparecerClass("retiradaS");
        aparecerClass("retiradaN");
    }
}

function changeDate(va){
    document.getElementById('dataT').value = va;
}

function pro(va){
    if (va === "0") {
        document.getElementById('problemDes').disabled=false;
        document.getElementById('noproblemDes').disabled=true;
    }else if (va === "1") {
        document.getElementById('problemDes').disabled=true;
        document.getElementById('noproblemDes').disabled=false;
    }
}

function addTryVisit() {
    if(contTryVisit > 0){
        $("#tryVisit").append('<hr>');
    }
    $("#tryVisit").append('<label>Data: <input type="date" name="dateTryVisit[' + contTryVisit + ']" id="dateTryVisit' + contTryVisit + '"/></label>\n' +
        '<label>Hora: <input type="time" name="hourTryVisit[' + contTryVisit + ']"/></label><br>\n' +
        '<div class="opRet hide"><label>Técnico: <textarea name="tecTryVisit[' + contTryVisit + ']"></textarea></label><br>\n' +
        'Retirou o equipamento? <label><input type="radio" name="retirTryVisit[' + contTryVisit + ']" value="1" onclick="desaparecerClass(\'notRet' + contTryVisit + '\')"> Sim</label>\n' +
        '<label><input type="radio" name="retirTryVisit[' + contTryVisit + ']" value="0" onclick="aparecerClass(\'notRet' + contTryVisit + '\')"> Não<br></label>\n' +
        '<label class="notRet' + contTryVisit + ' hide">Por que não retirou?<textarea name="reasonTryVisit[' + contTryVisit + ']"></textarea><br></label>\n' +
        '<div class="hide notRet' + contTryVisit + '">Vizinho soube dar informação?<label><input type="radio" name="vizinhoTryVisit[' + contTryVisit + ']" value="1" onclick="aparecerClass(\'infoTryVisit' + contTryVisit + '\')"> Sim</label>\n' +
        '<label><input type="radio" name="vizinhoTryVisit[' + contTryVisit + ']" value="0" onclick="desaparecerClass(\'infoTryVisit' + contTryVisit + '\')"> Não<br></label></div>\n' +
        '<label class="hide infoTryVisit' + contTryVisit + '"><div class="lab">Qual foi a informação?</div><textarea name="infoTryVisit[' + contTryVisit + ']"></textarea><br></label>\n' +
        '<label class="hide infoTryVisit' + contTryVisit + '"><div class="lab">Parentesco de quem deu<br>informação:</div> <textarea name="parTryVisit[' + contTryVisit + ']"></textarea><br></label>\n' +
        '<label class="hide infoTryVisit' + contTryVisit + '"><div class="lab">Endereço de quem deu<br> a informação:</div><textarea name="addressTryVisit[' + contTryVisit + ']"></textarea><br></label>\n' +
        '<label class="hide infoTryVisit' + contTryVisit + '">Falou com: <input name="vizinhoNameTryVisit[' + contTryVisit + ']" placeholder="Nome completo"><br></label></div>');
    contTryVisit++;
}

function addTryCall() {
    $("#tryCallAppend").append('<label>Telefone: <input class="tel" name="telTryCall[' + contTryCall + ']"><br></label>' +
        '<label><div class="lab">Resultado da<br>ligação</div><textarea name="resTryCall[' + contTryCall + ']"></textarea><br></label>' +
        '<label>Data: <input type="date" name="dateTryCall[' + contTryCall + ']" id="dateTryCall' + contTryCall + '"/></label>\n' +
        '<label>Hora: <input type="time" name="hourTryCall[' + contTryCall + ']"/></label><br>\n' +
        '<label>Funcionário: <textarea name="tecTryCall[' + contTryCall + ']"></textarea></label><br>\n' +
        '<label class="infoTryCall' + contTryCall + '">Falou com: <input name="nameTryCall[' + contTryCall + ']" placeholder="Nome completo"><br></label>' +
        '<label class="infoTryCall' + contTryCall + '"><div class="lab">Parentesco:</div> <input name="parTryCall[' + contTryCall + ']"><br></label><hr>\n');
    contTryCall++;
    $("input.tel").inputmask("(99) 9999[9]-9999").focusout(function (event) {
        var target, phone, element;
        target = (event.currentTarget) ? event.currentTarget : event.srcElement;
        phone = target.value.replace(/\D/g, '');
        element = $(target);
        if(phone.length > 10){
            element.inputmask("(99) 99999-999[9]");
        }else if(phone.length > 9){
            element.inputmask("(99) 9999-9999[9]");
        }else if(phone.length > 8){
            element.inputmask("(35) 99999-999[9]");
        }else if(phone.length > 7){
            element.inputmask("(35) 9999-9999[9]");
        }else{
            element.inputmask("(35) 9999-9999[9]");
        }
    }).focus(function (event) {
        var target, element;
        target = (event.currentTarget) ? event.currentTarget : event.srcElement;
        element = $(target);
        element.inputmask("(99) 9999[9]-9999");
    });
}

function addTec() {
    $("#tec").append("<br /><label><input name=\"tecExt[]\" list=\"lanc_tecnico\" style=\"margin-left: 143px; width: 300px\"/></label>");
}

$(document).ready(function () {
    $('input[type="checkbox"][name="tentativa"]').change(function() {
        if(this.checked) {
            aparecerClass('tentativa');
        }else{
            desaparecerClass('tentativa');
        }
    });
});
